import { Component, OnInit } from '@angular/core';  
import { ActivatedRoute, Router } from '@angular/router';
import { ILogin } from 'src/app/interfaces/login';  
import { FormBuilder, FormGroup, Validators } from '@angular/forms';  
import { first } from "rxjs/operators";
import swal from "sweetalert2";
import { Aeservice } from 'src/app/services/aes.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({  
   selector: 'app-login',  
   templateUrl: './login.component.html',  
   styleUrls: ['./login.component.css']  
   })  
export class LoginComponent implements OnInit {  
  // model: ILogin = { userid: "rohit", password: "12345678" }  
   loginForm: FormGroup;
   flag: boolean;
   showMessage: string;
   returnUrl: string;  
   submitted : boolean = false;
   isLoggedIn : boolean = false;
   loading = false;
   user:any;
   constructor(  
      private formBuilder: FormBuilder,  
      private router: Router,
      private route: ActivatedRoute,  
      private aesService:Aeservice, 
      private authenticationService: AuthenticationService,
   ) { }  
   ngOnInit() {  
      this.loginForm = this.formBuilder.group({
         userid: ['', [Validators.required, Validators.email]],
         password: ['', [Validators.required,Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]]
     });
     
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
   }  

get f() { return this.loginForm.controls; }  

login(loginForm: FormGroup) { 
  this.submitted= true;
   if (this.loginForm.invalid) {  
      return;  
   }  
   
   this.loading = true;
   localStorage.setItem("email",loginForm.value.userid);
      //  const encPassword=window.btoa(this.aesService.encryptData(loginForm.value.password))
        this.authenticationService.login(loginForm.value.userid,loginForm.value.password )
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/payment-form']);

                },
                error => {
                    this.loading = false;
                    this.showMessage = error;
                    this.flag = true;
                });
                //this.authenticationService.logout
                
  }  
}  