export class Authentication {
    access_token: string; 
    token_type: string;
    expires_in: number;
    scope: string;
    authorities: string[];
    jti: string;
    email: string;
    name: string;
    id:number;
    userId:number;
    roleId:number;
    role_name:string;
    active:number;
   
   
}
