import { Role } from "./role";

export class User {
  id: number;
  password: string;
  email: string;
  isActive: true;
  role: Role;
  access_token: string;
  resetToken: string;
}