export class Role {
    id: number;
    roleName: string;
    createdAt:string;
    updatedAt:string;
}