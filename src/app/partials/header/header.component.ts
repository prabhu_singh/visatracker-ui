import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';  
import { BehaviorSubject } from 'rxjs';
import { Authentication } from 'src/app/models/authentication';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private currentUserSubject: BehaviorSubject<Authentication>;
  isLoggedIn! : boolean;
  constructor(private router: Router) { }
  ngOnInit(): void {
  // alert(localStorage.getItem('isLoggedIn'));
  localStorage.getItem('isLoggedIn') == "true" ? this.isLoggedIn = true: this.isLoggedIn = false;
 //alert(localStorage.getItem('isLoggedIn'))
  }
  logout() {  
    // console.log('logout');
    // this.isLoggedIn = false;
    // localStorage.removeItem('isLoggedIn'); 
    // this.router.navigate(['/login']);  
    localStorage.removeItem('currentUser');
   // this.currentUserSubject.next(null);
    this.router.navigate(['/login']);
  }

}
