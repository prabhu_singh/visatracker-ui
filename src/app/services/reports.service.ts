import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
    
@Injectable({    
   providedIn: 'root'    
})    
export class ReportsService { 
   constructor(private http: HttpClient) { }    
   apiUrl = environment.serverUrl;

   allReportsFetch(){
     return this.http.get(this.apiUrl+'/api/getAllPaymentData');
   } 
   ReportsFetchByDates(formData){
    return this.http.post(this.apiUrl+'/api/getDataByDates',formData);
   }
} 