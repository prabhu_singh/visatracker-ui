import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Authentication } from '../models/authentication';
import { Router } from '@angular/router';



@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<Authentication>;
    public currentUser: Observable<Authentication>;
    apiUrl = environment.serverUrl;
    constructor(private http: HttpClient,private router: Router) {
        this.currentUserSubject = new BehaviorSubject<Authentication>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Authentication {
        return this.currentUserSubject.value;
    }

    login(userid,password) {
        return this.http.post<any>(this.apiUrl+'/oauth/token',this.getParams(userid,password),{ headers: this.getHeaders()})
            .pipe(map(auth => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                if (auth && auth.access_token) {
                    localStorage.setItem("email",userid);
                    localStorage.setItem("token",auth.access_token)
                    localStorage.setItem('currentUser', JSON.stringify(auth));
                    this.currentUserSubject.next(auth);
                }

                return auth;
            }));
    }
    private getHeaders() {
        return new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Authorization', 'Basic ' + window.btoa('ForexWebApp:Forex@2021'));
    }
    private getParams(username:string,password:string) {
        return new HttpParams()
            .set('grant_type', 'password')
            .set('scope', 'read write')
            .set('username', username)
            .set('password', password);
    }
    // logout() {
    //     localStorage.removeItem('currentUser');
    //     this.currentUserSubject.next(null);
    //     this.router.navigate(['/login']);
    // }
    public isLoggedInUser(){
        return localStorage.getItem("currentUser")!=null;
    }
    public isAdmin(){
        return localStorage.getItem("roleName")=='ADMIN';
    }
    // public isClient(){
    //     return localStorage.getItem("roleName")=='CLIENT';
    // }
    // public isKAM(){
    //     return localStorage.getItem("roleName")=='KAM';
    // }
    // public isRKAM(){
    //     return localStorage.getItem("roleName")=='RKAM';
    // }
    // public register(map:any): Observable<any> {
    //     const url = this.apiUrl+`/admin/register`;
    //     return this.http.post<any>(url,map); 
    //   }

      changePassword(email,formData) {
        return this.http.post(this.apiUrl +`/visa/changePassword/${email}`,formData);
      }  
}