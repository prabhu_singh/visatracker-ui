import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import * as forge from 'node-forge';

@Injectable({
  providedIn: 'root'
})
export class Aeservice {
 
  public getKey(){
    return 'UmtOTlJFOUNWMlZpUVhCdw==';
     }     
   public getSecret(){
   return 'UkU5Q1YyVmlRWEJ3T2taRA==';
   }
   

    public encryptData(encdata) {
      const key = window.atob(this.getKey())
      const iv =  CryptoJS.lib.WordArray.random(8).toString();
      const cipher = forge.cipher.createCipher('AES-GCM', key);
      cipher.start({
          iv: iv, 
           additionalData: window.atob(this.getSecret()),
           tagLength: 128 
      }); 
      cipher.update(forge.util.createBuffer(encdata));
      cipher.finish();
      const encrypted = cipher.output;
      const encodedB64 = forge.util.encode64(encrypted.data);
      const tag = cipher.mode.tag; 
      const tagB64 = forge.util.encode64(tag.data);
      const trasmitmsg = iv+tagB64+encodedB64;
      return trasmitmsg
    }

    public decryptData(encData) {
      const key = window.atob(this.getKey())
      const trasmitmsg=key+encData;
      const iv=trasmitmsg.substr(16,16);
      const additionalString = trasmitmsg.substring(32, 56);
      var tagg=forge.util.decode64(additionalString);
		  const cipherString = trasmitmsg.substring(56);
      var ciphertext = forge.util.decode64(cipherString);
      var input = forge.util.createBuffer(ciphertext);
      var decipher = forge.cipher.createDecipher('AES-GCM', key);
      decipher.start({
        iv: iv,
       additionalData:  window.atob(this.getSecret()), // optional
        tagLength: 128, // optional, defaults to 128 bits
        tag: tagg // authentication tag from encryption
      });
      decipher.update(input);
      var pass = decipher.finish();
      if(pass) {
        return decipher.output.toString('utf8')
    }
    else{
      return "";
    }

    }


  }