import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
    
@Injectable({    
   providedIn: 'root'    
})    
export class PaymentService { 
   constructor(private http: HttpClient) { }    
   apiUrl = environment.serverUrl;
     
   submitPaymentForm(formData){
     
    const headers = { 'Authorization': 'Bearer my-token', 'My-Custom-Header': 'custom' };
      
     return this.http.post(this.apiUrl+'/api/payment', formData);
      
   } 
} 