import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentformComponent } from "./containers/paymentform/paymentform.component";
import { SearchreportsComponent } from "./containers/searchreports/searchreports.component";
import { SearchreportsmasterComponent } from "./containers/searchreportsmaster/searchreportsmaster.component";
import { UpdatehreportsComponent } from "./containers/updatehreports/updatehreports.component";
import { ChangepasswordComponent } from "./containers/changepassword/changepassword.component";
import { LoginComponent } from "./pages/login/login.component";
import { AuthGuard } from '../app/gaurds/auth.guard';
import { HeaderComponent } from './partials/header/header.component';


const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },  
  { path: "login", component: LoginComponent },
  { path: 'payment-form', component: PaymentformComponent, canActivate : [AuthGuard] },  
  { path: "search-reports", component: SearchreportsComponent, canActivate : [AuthGuard] },
  { path: "change-password", component: ChangepasswordComponent, canActivate : [AuthGuard] },
  { path: "logout", component: HeaderComponent, canActivate : [AuthGuard] } 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
