import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './partials/header/header.component';
import { FooterComponent } from './partials/footer/footer.component';
import { PaymentformComponent } from './containers/paymentform/paymentform.component';
import { SearchreportsComponent } from './containers/searchreports/searchreports.component';
import { SearchreportsmasterComponent } from './containers/searchreportsmaster/searchreportsmaster.component';
import { UpdatehreportsComponent } from './containers/updatehreports/updatehreports.component';
import { ChangepasswordComponent } from './containers/changepassword/changepassword.component';
import { LoginComponent } from './pages/login/login.component';
import { SearchfilterPipe } from './searchfilter.pipe';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../app/gaurds/auth.guard';
import { StoreModule } from '@ngrx/store'; 
import { rootReducer } from 'src/reducers';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PaymentformComponent,
    SearchreportsComponent,
    SearchreportsmasterComponent,
    UpdatehreportsComponent,
    ChangepasswordComponent,
    LoginComponent,
    SearchfilterPipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,    //import here
    ReactiveFormsModule, //import here
    DataTablesModule,
    HttpClientModule,
    StoreModule.forRoot(rootReducer)
  ],
  providers: [AuthGuard,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
