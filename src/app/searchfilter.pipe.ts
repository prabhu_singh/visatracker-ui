import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }
  transform(items: any[], searchedKeyword: string): any[] {
    if (!items) return [];
    if (!searchedKeyword) return items;
    searchedKeyword = searchedKeyword.toLowerCase();
    return items.filter(it => {  
      return it.toString().toLowerCase().includes(searchedKeyword); // see here  

    });
  }
}
