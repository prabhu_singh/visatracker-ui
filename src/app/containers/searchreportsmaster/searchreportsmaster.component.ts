import { Component, OnInit } from '@angular/core';
interface data{
  id: Number;
  refrenceNumber: String;
  beneficiaryName: String;
  valueDate: string;
  amount:number;
  emailAddress:string;
  beanBankAccount:string;
  ifscCode:string;
  status:string;
  
}

@Component({
  selector: 'app-searchreportsmaster',
  templateUrl: './searchreportsmaster.component.html',
  styleUrls: ['./searchreportsmaster.component.css']
})
export class SearchreportsmasterComponent implements OnInit {
  
  reports: data[] = [
    {
        id: 1,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Rohit',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@outlook.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'

    },
    {
        id: 2,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Amit',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@gmail.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Active'
    },
    {
        id: 3,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Saurabh',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@forex.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'In progress'
    },
    {
        id: 4,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Vivek',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@travel.fcm.in',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
    {
        id: 5,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Tarun',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@hotmail.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
    {
        id: 4,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Vivek',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@yahoo.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
    {
        id: 5,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Tarun',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@facebook.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
    {
        id: 4,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Vivek',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@domain.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
    {
        id: 5,
        refrenceNumber: 'TESTHBCZ',
        beneficiaryName: 'Tarun',
        valueDate: '21/09/2021',
        amount: 123,
        emailAddress: 'rohitgautam131191@gemini.com',
        beanBankAccount: '12435GHDNDKEOO32423',
        ifscCode: 'SBIN00002387',
        status:'Pending'
    },
];
dtOptions: any = {};
  constructor() { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
      dom: 'Bfrtip',
      buttons: [
          'csv', 'excel', 'print'
      ]
    };
  }

}
