import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SearchreportsmasterComponent } from './searchreportsmaster.component';

describe('SearchreportsmasterComponent', () => {
  let component: SearchreportsmasterComponent;
  let fixture: ComponentFixture<SearchreportsmasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchreportsmasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchreportsmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
