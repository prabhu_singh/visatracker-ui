import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatehreportsComponent } from './updatehreports.component';

describe('UpdatehreportsComponent', () => {
  let component: UpdatehreportsComponent;
  let fixture: ComponentFixture<UpdatehreportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatehreportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatehreportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
