import { asLiteral } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import swal from "sweetalert2";


@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  changePassowrdForm: FormGroup;
  currentUserEmail: String;
  loading = false;
  loader = false;
 
  submitted: boolean = false;
  get f() {
    return this.changePassowrdForm.controls;
  }

  constructor(private formBuilder: FormBuilder,private authenticationService: AuthenticationService,private router: Router) { }

  ngOnInit(): void {
    this.changePassowrdForm = this.formBuilder.group({     
      newPassword: ['', [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,20}$/),Validators.minLength(8), Validators.maxLength(20)]],
      confirmPassword:['', Validators.required]
    },{validator: this.checkIfMatchingPasswords('newPassword', 'confirmPassword')});
  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
          passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({noxtEquivalent: true})
      }
      else {
          return passwordConfirmationInput.setErrors(null);
      }
    }
  }
  changepassword(){
    this.submitted=true;
    if(this.changePassowrdForm.invalid){     
      return;
    }
    const formData = new FormData();
    formData.append('newPassword', this.changePassowrdForm.get("newPassword").value);
    formData.append('confirmPassword', this.changePassowrdForm.get("confirmPassword").value);
    this.currentUserEmail = this.authenticationService.currentUserValue.email;
    if (this.changePassowrdForm.get('confirmPassword').value !== '' && this.changePassowrdForm.get('newPassword').value !== '' && this.changePassowrdForm.get('confirmPassword').value !== ''&& this.changePassowrdForm.get('newPassword').value=== this.changePassowrdForm.get('confirmPassword').value) {
      
      localStorage.getItem("email");
      this.authenticationService.changePassword(localStorage.getItem("email")
      ,formData)
        .subscribe(
          res => {
            
              swal.fire({
                title: "Password changed successfully",
                text:"Login with new credentials!",
                icon: "info",
                timer: 10000,
                });
                
          },
          error => {
               swal.fire({
                 text:"Password Update Failed",
              });
          });
 
      }
      else{
        swal.fire({
          text:"'New Password and Confirm Password Not Matched'",
          });
      }

  
    }
  
  
  }

