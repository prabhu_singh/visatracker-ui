import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import {  PaymentService } from "src/app/services/payment.service";
import swal from 'sweetalert2'

@Component({
  selector: 'app-paymentorm',
  templateUrl: './paymentform.component.html',
  styleUrls: ['./paymentform.component.css']
})

export class PaymentformComponent implements OnInit {
  @Input() abc!:boolean; 

  paymentForm: FormGroup;
  submitted: boolean = false;
  isLoggedIn:boolean=true;
  loader : boolean = false;
  get f() {
    return this.paymentForm.controls;
  }
  constructor(private formBuilder: FormBuilder, public paymentService : PaymentService) { }
  ngOnInit(): void {
    this.paymentForm = this.formBuilder.group({
      // currency:[''],
      employeeName: [''],
      branch: [''],
      displayCartNumber: ['', [Validators.required, Validators.pattern("^[A-Z0-9]*$"), Validators.minLength(11), Validators.maxLength(11)]],
      displayCartNumberRadio:[''],
      beneficiaryName: ['', Validators.required],
      amount: ['', [Validators.required,Validators.pattern("^[0-9]*$")]],
      email: [''],   
      email1: [''],
      email2: [''],   
      invoiceNumber: ['', Validators.required],
      beanBankAccountNumber: ['', Validators.required],
      ifscBankCode: ['', [Validators.required, Validators.pattern("^[A-Z]{4}0[A-Z0-9]{6}$"), Validators.minLength(11), Validators.maxLength(11)]],     
      
    });
    localStorage.getItem('isLoggedIn') == "true" ? this.isLoggedIn = true: this.isLoggedIn = false;
  }
  submitRequest(){    
    if(this.paymentForm.invalid){
      this.submitted = true;     
      return;
    }

    const formData = new FormData(); 
    formData.append('employeeName', this.paymentForm.get("employeeName").value)
    formData.append('branch', this.paymentForm.get("branch").value)
    formData.append('displayCartNumber', this.paymentForm.get("displayCartNumber").value)
    formData.append('displayCartNumberRadio', this.paymentForm.get("displayCartNumberRadio").value)    
    formData.append('beneficiaryName', this.paymentForm.get("beneficiaryName").value)
    formData.append('amount', this.paymentForm.get("amount").value)
    formData.append('email1', this.paymentForm.get("email1").value) 
    formData.append('email2', this.paymentForm.get("email2").value)   
    formData.append('invoiceNumber', this.paymentForm.get("invoiceNumber").value)  
    formData.append('beanBankAccountNumber', this.paymentForm.get("beanBankAccountNumber").value)  
    formData.append('ifscBankCode', this.paymentForm.get("ifscBankCode").value)  
    this.paymentService.submitPaymentForm(formData)
      .subscribe(res => { 
        swal.fire({
          text: "Form Submitted Successfully.",
        });    
        this.paymentForm.reset();   
        this.submitted = false;   
      },
        error => {
          this.loader=false;
          if (error) {
            swal.fire({
              text: "Form Submission Failed.",
            });
          }
        });



  }

}
