import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportsService } from 'src/app/services/reports.service';
import swal from 'sweetalert2';
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-searchreports',
  templateUrl: './searchreports.component.html',
  styleUrls: ['./searchreports.component.css']
})
export class SearchreportsComponent implements OnInit {
  
  filterReportsForm!:FormGroup;
  reports : any;
  dtOptions: any = {};
  submitted : boolean = false;
  flag : boolean = false;
  get f() {
    return this.filterReportsForm.controls;
  }
  constructor(private formBuilder: FormBuilder, private reportsService : ReportsService, private datePipe : DatePipe) { }

  ngOnInit(): void {
    this.fetchAllData();  

    this.filterReportsForm = this.formBuilder.group({
      fromDate: ['',Validators.required],
      toDate: ['',Validators.required]     
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      dom: 'Bfrtip',
      buttons: [
          'csv', 'excel', 'print'
      ]
    };


  }
  //Fetch data by FromDate to ToDate method
  filterReports(){
    this.submitted = true;
    let a;
    let b;
    if(this.filterReportsForm.invalid){
      return;
    }

    const formData = new FormData(); 
    formData.append('fromDate', this.filterReportsForm.get("fromDate").value)
    formData.append('toDate', this.filterReportsForm.get("toDate").value);
    this.reportsService.ReportsFetchByDates(formData)
    .subscribe(
      data => {        
        if (data != null ) {
          this.reports =  data; 
          this.flag = true;
          console.log(JSON.stringify(this.reports));
        } else {
          setTimeout(function () {         
          }.bind(this), 3000);
         
        }
      },
      error => {
        swal.fire({
          text: error,
        });
          
      });
  }

  //Fetch data on page load
   fetchAllData(){    
    this.reportsService.allReportsFetch()
    .subscribe(
      data => {        
        if (data != null ) {
          this.reports = data; 
          this.flag = true;
          console.log(JSON.stringify(this.reports));
        } else {
          setTimeout(function () {         
          }.bind(this), 3000);
         
        }
      },
      error => {
        swal.fire({
          text: error,
        });
          
      });
   }

}
