import { Action } from "@ngrx/store";
import { UserListRequestAction, USER_LIST_REQUEST } from "src/actions/user-action";

export interface UserReducerState{
 isLoggedInNow : boolean; 
}

const initialState:UserReducerState = {
    isLoggedInNow : false, 
}

export function UserReducer(state = initialState, action :Action):UserReducerState{
    switch(action.type){
        case USER_LIST_REQUEST: {      
            alert('came here')    
            return {...state, isLoggedInNow : true}
        }
        default:{
            return state; 
        }
    }
   
}

//selectors

export const getisLoggedInNow = (state:UserReducerState) => state.isLoggedInNow;
