import { ActionReducerMap, createSelector } from "@ngrx/store";
import * as fromUser from "./user-reducers";

export interface RootReducersState{
    users : fromUser.UserReducerState
}

export const rootReducer : ActionReducerMap<RootReducersState> = {
users : fromUser.UserReducer,
}

export const getUserState = (state:RootReducersState) => state.users;
export const getisLoggedInNow = createSelector(getUserState, fromUser.getisLoggedInNow); 